NetworksPG4
by Crystal Colon, Molly Zachlin, and Auna Walton
Creating a Network Pong Game :)
Included file names

netpong.cpp - contains both host and client handlers
Makefile - compliation
Example commands to run code

User Host: ./netpong --host <portnum>
User Client: ./netpong student<num>.cse.nd.edu <portnum>
